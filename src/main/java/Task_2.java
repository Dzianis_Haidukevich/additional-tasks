import java.util.Scanner;

public class Task_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double R = scanner.nextDouble();
        double area = (Math.PI * Math.pow(R, 2));
        double len = (2 * Math.PI * R);
        System.out.println("Area of the circle = " + area);
        System.out.println("Circumference of the circle = " + len);
    }
}

